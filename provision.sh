#!/usr/bin/env bash

echo "[info] apt-get update"
sudo apt-get update

echo "[info] Installing add-apt-repository"
sudo apt-get install -y python-software-properties #--fix-missing
echo "[info] Installing ppa:git-core/ppa"
sudo add-apt-repository -y ppa:git-core/ppa
echo "[info] Installing ppa:webupd8team/java"
sudo add-apt-repository -y ppa:webupd8team/java

echo "[info] apt-get update"
sudo apt-get update

echo "[info] Preparing Oracle Java Silent installation"
echo debconf shared/accepted-oracle-license-v1-1 select true | \
sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
sudo debconf-set-selections
echo "[info] Installing Oracle Java 7"
sudo apt-get install -y oracle-java7-installer

echo "[info] Installing git"
apt-get install -y git

echo "[info] Installing maven 3"
apt-get install -y maven

echo "[info] Installing jenkins"
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install -y jenkins